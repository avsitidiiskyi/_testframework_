from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from variable_storage import *


def chrome_setup():
    driver = webdriver.Chrome(chrome)
    driver.maximize_window()
    return driver


def firefox_setup():
    driver = webdriver.Firefox(firefox)
    driver.maximize_window()
    return driver


class Init:

    def __init__(self, driver):
        self.driver = driver

    def find_element(self, locator, time=999):
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=999):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def locate_element(self, locator, time=999):
        return WebDriverWait(self.driver, time).until(EC.element_to_be_clickable(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def open_url(self, url):
        return self.driver.get(url)

    def open_new_url(self, new_url):
        return self.driver.execute_script(f"window.open('{new_url}', 'new_window')")

    def switch_to_window(self, index):
        return self.driver.switch_to_window(self.driver.window_handles[index])




