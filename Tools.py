#Female/Male name depending on random selection
import random
import csv
from string import ascii_letters
from random import choice
from variable_storage import *


def get_word_list_from_file(file_name):
    with open(file_name, "r", encoding='utf-8') as f:
        csv_reader = csv.reader(f)
        ruskie_list = [line[0] for line in csv_reader]
    return ruskie_list
def ruskie_name_male():
    return get_word_list_from_file("ruskie_name_male.csv")
def ruskie_name_FEmale():
    return get_word_list_from_file("ruskie_name_FEmale.csv")
def ruskie_surname_male():
    return get_word_list_from_file("ruskie_surname_male.csv")
def ruskie_surname_FEmale():
    return get_word_list_from_file("ruskie_surname_FEmale.csv")
def get_random_male_name():
    return random.choice(ruskie_name_male())
def get_random_FEmale_name():
    return random.choice(ruskie_name_FEmale())
def get_random_male_surname():
    return random.choice(ruskie_surname_male())
def get_random_FEmale_surname():
    return random.choice(ruskie_surname_FEmale())
def get_random_name():
    return random.choice([get_random_male_name(), get_random_FEmale_name()])
def get_random_surname_for_given_name(name):
    if name in ruskie_name_male():
        return get_random_male_surname()
    return get_random_FEmale_surname()


def magic():
    random_name = get_random_name()
    random_surname = get_random_surname_for_given_name(random_name)
    return random_name, random_surname
name, surname = magic()


#For Credentials

def name_for_email():
    with open("names.csv", "r") as csv_file:
        csv_reader = csv.reader(csv_file)
        name_list = []
        for line in csv_reader:
            name_list.append(line[0])

    final_name = random.choice(name_list)
    return final_name

 #_______________________EMAIL FUNCTION______________________________________


def e_mail():
    first_email_part = (''.join(choice(ascii_letters) for i in range(4)))
    second_email_part = name_for_email()
    final_part = random.choice(domains)
    result = str(first_email_part) + str(second_email_part) + str(final_part)
    return result


# #_________________Phone_function_________________
def phone():
    var_code = [96, 50, 97, 98, 73, 99]
    code = random.choice(var_code)
    num = (random.randrange(9000000))
    result = str(code) + str(num)
    return result


#____________________PASSWORD______________________________#
def password():
    a = (''.join(choice(ascii_letters) for i in range(5)))
    num = (random.randrange(900))

    spec = (random.choice(special))
    return a + str(spec) + str(num)

#________________read\write___________________#


def password_writer(file):
    with open("password.txt", 'w') as writeable:
        writeable.write(file)


def login_writer(file):
    with open("login.txt", 'w') as writeable:
        writeable.write(file)


def reader(readable_file):
    with open(readable_file) as readable:
        read = readable.read()
        return read









