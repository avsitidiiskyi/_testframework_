import unittest
import requests
from urls import StatusCodes, Urls


class SimpleTestExample(unittest.TestCase):
    def test_status_codes(self):
        response = requests.get(StatusCodes.selenium)
        self.assertEqual(response.ok, True)

    def test_status_codes1(self):
        response = requests.get(StatusCodes.about)
        self.assertEqual(response.ok, True)

    def test_status_codes2(self):
        response = requests.get(StatusCodes.event)
        self.assertEqual(response.ok, True)

    def test_status_codes3(self):
        response = requests.get(StatusCodes.project)
        self.assertEqual(response.ok, True)

    def test_status_codes4(self):
        response = requests.get(Urls.alpari)
        self.assertEqual(response.ok, True)

    def test_status_codes5(self):
        response = requests.get(Urls.site)
        self.assertEqual(response.ok, True)


if __name__ == '__main__':
    unittest.main()
