from Initilization import Init


class Seeker(Init):

    def clicker(self, locator):
        action = self.find_element(locator, time=2).click()
        return action

    def typewrite(self, word, locator):
        search_field = self.find_element(locator)
        search_field.send_keys(word)
        return search_field

    def upload(self, address, locator):
        element = self.find_element(locator)
        element.send_keys(address)
        return element

    def get_current_url(self):
        return self.driver.current_url

    def get_text_element(self, locator):
        action = self.find_element(locator, time=2)
        return action.text


