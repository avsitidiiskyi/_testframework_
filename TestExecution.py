from Initilization import chrome_setup, firefox_setup
from urls import Urls
from Methods import Seeker
from Locators import Locators
from time import sleep
from Tools import *


class TestExecution(Locators, Seeker, Urls):
    @staticmethod
    def launch(browser):
        _try_ = Seeker(browser)
        _try_.open_url(Urls.site)
        sleep(2)
        _try_.typewrite('Selenium', Locators.SearchField)
        sleep(2)
        _try_.clicker(Locators.GO)
        _try_.open_new_url(Urls.selenium)
        _try_.switch_to_window(1)
        sleep(5)



if __name__ == "__main__":
    User = TestExecution
    driver = chrome_setup()
    User.launch(driver)
    try:
        assert "Started" in driver.page_source
    except AssertionError:
        print('Started NOT LOCATED')

    sleep(1)
    driver.quit()


